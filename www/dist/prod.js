(function() {
  angular.module('karmacar.confirm', []);
})();

(function() {
  angular.module('karmacar.editprofile', []);
})();

(function() {
  angular.module('karmacar.makeoffer', []);
})();

(function() {
  angular.module('karmacar.myriderequests', []);
})();

(function() {
  angular.module('karmacar.profilewidget', []);
})();

(function() {
  angular.module('karmacar.requestlist', []);
})();

(function() {
  angular.module('karmacar.requestmap', []);
})();

(function() {
  angular.module('karmacar.rating', []);
})();

(function() {
  angular.module('karmacar.ridehistory', []);
})();

(function() {
  angular.module('karmacar.rideoffers', []);
})();

(function() {
  angular.module('karmacar.serviceModule', []);
})();

(function() {
  angular.module('karmacar.viewrequest', []);
})();

(function() {
  angular.module('karmacar.riderequest', []);
})();

(function() {
  angular.module('karmacar.confirm')
    .controller('confirm-controller', [confirmController]);

  function confirmController() {
    //a;sdlfias;difu
    console.log("testing");
  }
})();

(function() {
  angular.module('karmacar.editprofile')
    .controller('editprofile-controller', ['$scope', '$http', editProfileController]);

  function editProfileController($scope, $http) {

    $scope.updated = false;

    $scope.updateUsername = function() {
      if(this.name) {
        $scope.profiles[0].name = this.name;
        this.name = "";
        $scope.updated = true;
      }
    };

    $scope.updateCarModel = function() {
      if(this.carModel) {
        $scope.profiles[0].carModel = this.carModel;
        this.carModel = "";
        $scope.updated = true;

      }
    };

    $scope.addInterest = function() {
      if(this.interest) {
        $scope.profiles[0].interests.push(this.interest);
        this.interest = "";
        $scope.updated = true;

      }
    };

    function initialize() {
      $http.get('../mockdata/profile.json').then(function(result) {
        $scope.profiles = result.data.Profiles;
      });
    }

    initialize();
  }
})();

(function() {
  angular.module('karmacar.makeoffer')
    .controller('makeoffer-controller', ['$scope', '$state', makeOfferController])
    .directive('standardTimeMeridian', function() {
    return {
      restrict: 'AE',
      replace: true,
      scope: {
        etime: '=etime'
      },
      template: "<strong>{{stime}}</strong>",
      link: function(scope, elem, attrs) {

        scope.stime = epochParser(scope.etime, 'time');

        function prependZero(param) {
          if (String(param).length < 2) {
            return "0" + String(param);
          }
          return param;
        }

        function epochParser(val, opType) {
          if (val === null) {
            return "00:00";
          } else {
            var meridian = ['AM', 'PM'];

            if (opType === 'time') {
              var hours = parseInt(val / 3600);
              var minutes = (val / 60) % 60;
              var hoursRes = hours > 12 ? (hours - 12) : hours;

              var currentMeridian = meridian[parseInt(hours / 12)];

              return (prependZero(hoursRes) + ":" + prependZero(minutes) + " " + currentMeridian);
            }
          }
        }

        scope.$watch('etime', function(newValue, oldValue) {
          scope.stime = epochParser(scope.etime, 'time');
        });

      }
    };
  });

  function makeOfferController($scope, $state) {

    $scope.timePickerObject = {
      inputEpochTime: ((new Date()).getHours() * 60 * 60),  //Optional
      step: 15,  //Optional
      format: 12,  //Optional
      titleLabel: '12-hour Format',  //Optional
      setLabel: 'Set',  //Optional
      closeLabel: 'Close',  //Optional
      setButtonType: 'button-positive',  //Optional
      closeButtonType: 'button-stable',  //Optional
      callback: function (val) {    //Mandatory
        timePickerCallback(val);
      }
    };

    function timePickerCallback(val) {
      if (typeof (val) === 'undefined') {
        console.log('Time not selected');
      } else {
        var selectedTime = new Date(val * 1000);
        $scope.timePickerObject.inputEpochTime = val;
        console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), ':', selectedTime.getUTCMinutes(), 'in UTC');
      }
    }

    $scope.submitOffer = function(){
      $state.go('rideoffers');
    };

    $scope.pickupLoactionChanged =function(val) {
      console.dir(val);
    };
  }
})();

(function() {
  angular.module('karmacar.myriderequests')
    .controller('myRideRequests-controller', ['$scope', myRideRequestsController]);

  function myRideRequestsController($scope) {

    $scope.toggleWidget = true;

  }
})();

(function() {
  angular.module('karmacar.profilewidget')
    .controller('profilewidget-controller', ['$scope', '$state', 'userService', profileWidgetController]);

  function profileWidgetController($scope, $state, userService) {
    $scope.profile = userService.scope.status;
    $scope.requestInfo = userService.scope.status;

    $scope.generateStars = function() {
      var karmaPoints = [];
      for(var i = 0; i < $scope.profile.showProfile.karma / 10; i++) {
        karmaPoints.push(i);
      }
      return karmaPoints;
    };
  }
})();

(function() {
  function profilewidget() {
    return {
      templateUrl: 'templates/modules/profilewidget/profilewidget.html',
      controller: 'profilewidget-controller'
    }
  }

  angular.module('karmacar.profilewidget')
    .directive('profilewidget', [profilewidget]);
})();

(function() {
  angular.module('karmacar.requestlist')
    .controller('requestlist-controller', ['$scope', '$http', requestListController]);

  function requestListController($scope, $http) {
    $scope.requests = {};

    function initialize() {
      $http.get('../mockdata/requestlist.json').then(function(result) {
        $scope.requests = result.data.Requests;
      });
    }

    $scope.generateStars = function(requestUser) {
      var karmaPoints = [];
      for(var i = 0; i < requestUser.requesteeProfile.karma / 10; i++) {
        karmaPoints.push(i);
      }
      return karmaPoints;
    };

    initialize();
  }
})();

(function() {
  angular.module('karmacar.requestmap')
    .controller('requestmap-controller', [requestMapController]);

  function requestMapController() {
    //a;sdlfias;difu
    console.log("testing");
  }
})();

(function() {
  angular.module('karmacar.rating')
    .controller('rating-controller', [ratingController]);

  function ratingController() {

  }
})();

(function() {
  angular.module('karmacar.ridehistory')
    .controller('ridehistory-controller', [rideHistoryController]);

  function rideHistoryController() {

  }
})();

(function() {
  angular.module('karmacar.rideoffers')
    .controller('rideoffers-controller', ['$scope', rideOffersController]);

  function rideOffersController($scope) {

    $scope.toggleWidget = true;

  }
})();

(function() {
  angular.module('karmacar.serviceModule')
    .factory('requestService', ['userService', requestService]);

  function requestService(userService) {

    var submitRequest = function(rideRequest){
      userService.scope.myRideRequests.push(rideRequest);

    };

    var submitOffer = function(){

    };

    var acceptOffer = function(){

    };

    var confirmOffer = function(){

    };

    return {
      submitRequest: submitRequest,
      submitOffer: submitOffer,
      acceptOffer: acceptOffer,
      confirmOffer: confirmOffer
    };

  }

})();

(function() {
  angular.module('karmacar.serviceModule')
    .factory('userService', [ userService]);

  function userService() {

    var scope = {};
    scope.status ={};
    scope.profile={};
    scope.myRideRequests =[];
    scope.myRideOffers =[];

    return {
      scope: scope
    };

  }

})();

(function() {
  angular.module('karmacar.viewrequest')
    .controller('viewrequest-controller', ['$scope', '$stateParams', '$http', '$ionicHistory', 'userService', viewRequestController]);

  function viewRequestController($scope, $stateParams, $http, $ionicHistory, userService) {
    $scope.request ={};
    userService.scope.status.showProfile = {};

    function initialize() {
      $http.get('../mockdata/requestlist.json').then(function(result) {
        $scope.requests = result.data.Requests;
      }).then(function() {
        $scope.requests.forEach(function(request) {
          if(request.id === $stateParams.id) {
            $scope.request = request;
            userService.scope.status.showProfile = request.requesteeProfile;
            userService.scope.status.userRequest = request;
          }
        });
      });
    }

    $scope.goBack = function() {
      $ionicHistory.goBack();
    };

    initialize();
  }
})();

(function() {
  angular.module('karmacar.riderequest')
    .controller('riderequest-controller', ['$scope', '$state',rideRequestController]);

  function rideRequestController($scope, $state) {


    $scope.submitRequest = function(){
      $state.go('myriderequests');
    };

    $scope.fromLocationChanged =function(val) {
      console.dir(val);
    };

    $scope.toLocationChanged =function(val) {
      console.dir(val);
    };


    function timePickerCallback(val) {
      if (typeof (val) === 'undefined') {
        console.log('Time not selected');
      } else {
        var selectedTime = new Date(val * 1000);
        console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), ':', selectedTime.getUTCMinutes(), 'in UTC');
      }
    }

    $scope.startTimePicker = {
      inputEpochTime: ((new Date()).getTime() * 60 * 60),  //Optional
      step: 5,  //Optional
      format: 12,  //Optional
      titleLabel: 'Soonest available for ride',  //Optional
      setLabel: 'Select',  //Optional
      closeLabel: 'Close',  //Optional
      setButtonType: 'button-positive',  //Optional
      closeButtonType: 'button-stable',  //Optional
      callback: function (val) {    //Mandatory
        timePickerCallback(val);
      }
    };

    $scope.endTimePicker = {
      inputEpochTime: ((new Date()).getTime() * 60 * 60),  //Optional
      step: 5,  //Optional
      format: 12,  //Optional
      titleLabel: 'After this time I nolonger need a ride',  //Optional
      setLabel: 'Select',  //Optional
      closeLabel: 'Close',  //Optional
      setButtonType: 'button-positive',  //Optional
      closeButtonType: 'button-stable',  //Optional
      callback: function (val) {    //Mandatory
        timePickerCallback(val);
      }
    };

  }
})();

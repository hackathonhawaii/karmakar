(function() {
    angular.module('karmacar', ['ionic','ion-place-tools', 'ionic-timepicker',
    'karmacar.editprofile',
    'karmacar.profilewidget',
    'karmacar.rideoffers',
    'karmacar.offerview',
          'karmacar.riderequest',
            'karmacar.myriderequests',
            'karmacar.myrideoffers',
            'karmacar.ridehistory',
            'karmacar.rating',
            'karmacar.requestlist',
            'karmacar.requestmap',
            'karmacar.makeoffer',
            'karmacar.viewrequest',
            'karmacar.confirm',
            'karmacar.serviceModule',
            'karmacar.rateuser',
      'karmacar.home'
        ]);

          angular.module('karmacar').run(function($ionicPlatform) {
            $ionicPlatform.ready(function() {
              // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
              // for form inputs)
              if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
          // org.apache.cordova.statusbar required
          StatusBar.styleDefault();
        }
      });
    })

    .config(function($stateProvider, $urlRouterProvider) {

      // Ionic uses AngularUI Router which uses the concept of states
      // Learn more here: https://github.com/angular-ui/ui-router
      // Set up the various states which the app can be in.
      // Each state's controller can be found in controllers.js
      $stateProvider

      // setup an abstract state for the tabs directive
        .state('home', {
          url: '/home',
          templateUrl: 'templates/modules/homescreen/home.html',
          controller: 'home-controller'
        })
        .state('editProfile', {
          url: '/editprofile',
          templateUrl: 'templates/modules/editprofile/editprofile.html',
          controller: 'editprofile-controller'
        })
        .state('profilewidget', {
          url: '/profilewidget',
          templateUrl: 'templates/modules/profilewidget/profilewidget.html',
          controller: 'profilewidget-controller'
        })
        .state('rideoffers', {
          url: '/rideoffers',
          templateUrl: 'templates/modules/rideoffers/rideoffers.html',
          controller: 'rideoffers-controller'
        })
        .state('offerview', {
          url: '/offerview',
          templateUrl: 'templates/modules/rideoffers/offerview.html',
          controller: 'offerview-controller'
        })
        .state('riderequest', {
          url: '/riderequest',
          templateUrl: 'templates/modules/riderequest/riderequest.html',
          controller: 'riderequest-controller'
        })
        .state('myriderequests', {
          url: '/myriderequests',
          templateUrl: 'templates/modules/myriderequests/myriderequests.html',
          controller: 'myRideRequests-controller'
        })
        .state('myrideoffers', {
          url: '/myrideoffers',
          templateUrl: 'templates/modules/myrideoffers/myrideoffers.html',
          controller: 'myrideoffers-controller'
        })
        .state('ridehistory', {
          url: '/ridehistory',
          templateUrl: 'templates/modules/ridehistory/ridehistory.html',
          controller: 'ridehistory-controller'
        })
        .state('rating', {
          url: '/rating',
          templateUrl: 'templates/modules/ridehistory/rating.html',
          controller: 'rating-controller'
        })
        .state('requestList', {
          url: '/requestlist',
          templateUrl: 'templates/modules/requestlist/requestlist.html',
          controller: 'requestlist-controller'
        })
        .state('requestMap', {
          url: '/requestmap',
          templateUrl: 'templates/modules/requestmap/requestmap.html',
          controller: 'requestmap-controller'
        })
        .state('makeOffer', {
          url: '/makeoffer',
          templateUrl: 'templates/modules/makeoffer/makeoffer.html',
          controller: 'makeoffer-controller'
        })
        .state('viewRequest', {
          url: '/viewrequest/:id',
          templateUrl: 'templates/modules/viewrequest/viewrequest.html',
          controller: 'viewrequest-controller'
        })
        .state('confirm', {
          url: '/confirm',
          templateUrl: 'templates/modules/confirm/confirm.html',
          controller: 'confirm-controller'
        })
        .state('rateuser', {
          url: '/rateuser',
          templateUrl: 'templates/modules/rateuser/rateuser.html',
          controller: 'rateuser-controller'
        });


      // if none of the above states are matched, use this as the fallback
      $urlRouterProvider.otherwise('/home');
    });
})();

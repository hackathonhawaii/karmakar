(function() {
  angular.module('karmacar.editprofile')
    .controller('editprofile-controller', ['$scope', '$http', '$state', 'userService', editProfileController]);

  function editProfileController($scope, $http, $state, userService) {

    $scope.profile = userService.scope.profile;

    $scope.addInterest = function() {
      if(this.interest) {
        $scope.profile.interests.push(this.interest);
        this.interest = "";
      }
    };

    $scope.saveChanges = function() {
      userService.scope.profile = $scope.profile;
      $state.go('home');
    }
  }
})();

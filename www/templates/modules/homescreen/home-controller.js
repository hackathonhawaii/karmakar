(function() {
  angular.module('karmacar.home')
    .controller('home-controller', ['$scope', '$state', 'userService', homeController])


  function homeController($scope, $state, userService) {

    $scope.offers = userService.scope.myRideOffers;

    $scope.requests = userService.scope.myRideRequests;



  }
})();

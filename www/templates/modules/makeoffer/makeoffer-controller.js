(function() {
  angular.module('karmacar.makeoffer')
    .controller('makeoffer-controller', ['$scope', '$state', 'requestService', makeOfferController])
    .directive('standardTimeMeridian', function() {
    return {
      restrict: 'AE',
      replace: true,
      scope: {
        etime: '=etime'
      },
      template: "<strong>{{stime}}</strong>",
      link: function(scope, elem, attrs) {

        scope.stime = epochParser(scope.etime, 'time');

        function prependZero(param) {
          if (String(param).length < 2) {
            return "0" + String(param);
          }
          return param;
        }

        function epochParser(val, opType) {
          if (val === null) {
            return "00:00";
          } else {
            var meridian = ['AM', 'PM'];

            if (opType === 'time') {
              var hours = parseInt(val / 3600);
              var minutes = (val / 60) % 60;
              var hoursRes = hours > 12 ? (hours - 12) : hours;

              var currentMeridian = meridian[parseInt(hours / 12)];

              return (prependZero(hoursRes) + ":" + prependZero(minutes) + " " + currentMeridian);
            }
          }
        }

        scope.$watch('etime', function(newValue, oldValue) {
          scope.stime = epochParser(scope.etime, 'time');
        });

      }
    };
  });

  function makeOfferController($scope, $state, requestService) {

    $scope.offer ={};

    $scope.timePickerObject = {
      inputEpochTime: ((new Date()).getHours() * 60 * 60),  //Optional
      step: 15,  //Optional
      format: 12,  //Optional
      titleLabel: '12-hour Format',  //Optional
      setLabel: 'Set',  //Optional
      closeLabel: 'Close',  //Optional
      setButtonType: 'button-positive',  //Optional
      closeButtonType: 'button-stable',  //Optional
      callback: function (val) {    //Mandatory
        timePickerCallback(val);
      }
    };

    $scope.formatEpochTime = function(epoch) {
      var objDate = new Date(epoch * 1000);
      var meridian = (objDate.getUTCHours() >= 12) ? "PM" : "AM";
      var hours = (objDate.getUTCHours() > 12) ? ((objDate.getUTCHours() - 12)) : (objDate.getUTCHours());
      var minutes = (objDate.getUTCMinutes());

      minutes = (minutes < 10) ? ("0" + minutes) : (minutes);

      if (hours === 0 && meridian === "AM") {
        hours = 12;
      }
      console.log(hours + ':' + minutes + meridian);
      return(hours + ':' + minutes + meridian);
    };

    function timePickerCallback(val) {
      if (typeof (val) === 'undefined') {
        console.log('Time not selected');
      } else {
        var selectedTime = new Date(val * 1000);
        $scope.timePickerObject.inputEpochTime = val;
        $scope.offer.pickUpTime = $scope.formatEpochTime(val);
        console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), ':', selectedTime.getUTCMinutes(), 'in UTC');
      }
    }

    $scope.submitOffer = function(){
      $scope.offer.id = $scope.length + 1;
      requestService.submitOffer($scope.offer);
      $state.go('myrideoffers');
    };

    $scope.pickupLoactionChanged =function(val) {
      console.dir(val);
    };
  }
})();

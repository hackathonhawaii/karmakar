(function() {
  angular.module('karmacar.myrideoffers')
    .controller('myrideoffers-controller', ['$scope','$state', 'userService', 'requestService', myRideOffersController]);

  function myRideOffersController($scope, $state, userService, requestService) {

    $scope.offers = {};
    $scope.selectedRide = {};

    function initialize() {
      $scope.offers = userService.scope.myRideOffers;
      console.dir(userService.scope.myRideOffers);
    }

    $scope.generateStars = function(requestUser) {
      var karmaPoints = [];
      for(var i = 0; i < requestUser.karma / 10; i++) {
        karmaPoints.push(i);
      }
      return karmaPoints;
    };

    $scope.setSelection = function(selection) {
      $scope.selectedRide = angular.copy(selection);
    };


    $scope.cancelOffer = function(offer) {
      requestService.cancelOffer(offer);
    };

    $scope.acceptOffer = function(offer) {
      requestService.acceptOffer(offer);
    };

    $scope.rateUser = function() {
      $state.go('rateuser');
    };

    initialize();

  }
})();

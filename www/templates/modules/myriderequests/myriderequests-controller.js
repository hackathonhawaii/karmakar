(function() {
  angular.module('karmacar.myriderequests')
    .controller('myRideRequests-controller', ['$scope', '$http', '$state', 'userService', myRideRequestsController]);

  function myRideRequestsController($scope, $http, $state, userService) {

    $scope.selectedRide = {};

    $scope.requests = {};

    function initialize() {
      $scope.requests = angular.copy(userService.scope.myRideRequests);
    }
    initialize();

    $scope.generateStars = function(requestUser) {
      var karmaPoints = [];
      for(var i = 0; i < requestUser.karma / 10; i++) {
        karmaPoints.push(i);
      }
      return karmaPoints;
    };

    $scope.setSelection = function(selection) {
      $scope.selectedRide = angular.copy(selection);
      userService.scope.selectedMyRequest = angular.copy(selection);
      $state.go('rideoffers');

    }
  }
})();

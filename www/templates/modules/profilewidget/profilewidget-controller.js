(function() {
  angular.module('karmacar.profilewidget')
    .controller('profilewidget-controller', ['$scope', '$state', '$http', 'userService', profileWidgetController]);

  function profileWidgetController($scope, $state, $http, userService) {
    $scope.profile = userService.scope.status;
    $scope.requestInfo = userService.scope.status;

    $scope.requests = {};

    function initialize() {
      $http.get('../mockdata/requestlist.json').then(function(result) {
        $scope.requests = result.data.Requests;
      });
    }
    initialize();

    $scope.generateStars = function() {
      var karmaPoints = [];
      for(var i = 0; i < $scope.profile.showProfile.karma / 10; i++) {
        karmaPoints.push(i);
      }
      return karmaPoints;
    };
  }
})();

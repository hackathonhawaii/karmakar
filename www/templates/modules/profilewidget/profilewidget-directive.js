(function() {
  function profilewidget() {
    return {
      templateUrl: 'templates/modules/profilewidget/profilewidget.html',
      controller: 'profilewidget-controller'
    }
  }

  angular.module('karmacar.profilewidget')
    .directive('profilewidget', [profilewidget]);
})();

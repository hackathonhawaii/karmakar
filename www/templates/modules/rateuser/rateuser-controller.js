(function() {
  angular.module('karmacar.rateuser')
    .controller('rateuser-controller', ['$scope', '$state', rateUserController]);

  function rateUserController($scope, $state) {

    $scope.submitReview = function() {
      $state.go('home');
    }
  }
})();

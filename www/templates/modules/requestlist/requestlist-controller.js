(function() {
  angular.module('karmacar.requestlist')
    .controller('requestlist-controller', ['$scope', '$http', requestListController]);

  function requestListController($scope, $http) {
    $scope.requests = {};

    function initialize() {
      $http.get('../mockdata/requestlist.json').then(function(result) {
        $scope.requests = result.data.Requests;
      });
    }

    $scope.generateStars = function(requestUser) {
      var karmaPoints = [];
      for(var i = 0; i < requestUser.requesteeProfile.karma / 10; i++) {
        karmaPoints.push(i);
      }
      return karmaPoints;
    };

    initialize();
  }
})();

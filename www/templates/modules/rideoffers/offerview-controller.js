(function() {
  angular.module('karmacar.offerview')
    .controller('offerview-controller', ['$scope','$http','$state', offerViewController]);

  function offerViewController($scope, $http, $state) {

    $scope.requests = {};

    $scope.selectedRide = {};

    function initialize() {
      $http.get('../mockdata/requestlist.json').then(function(result) {
        $scope.requests = result.data.Requests;
      });
    }
    initialize();

    $scope.submitRequest = function(){
      $state.go('confirm');
    };

    $scope.generateStars = function(requestUser) {
      var karmaPoints = [];
      for(var i = 0; i < requestUser.karma / 10; i++) {
        karmaPoints.push(i);
      }
      return karmaPoints;
    };

    $scope.setSelection = function(selection) {
      $scope.selectedRide = angular.copy(selection);
     userService.scope.selectedRequest = angular.copy(selection);

    }

  }
})();
/**
 * Created by Garry on 3/12/2016.
 */

(function() {
  angular.module('karmacar.riderequest')
    .controller('riderequest-controller', ['$scope', '$state', 'requestService',rideRequestController]);

  function rideRequestController($scope, $state, requestService) {

    $scope.request ={};

    $scope.startTime = "--:-- AM/PM";
    $scope.endTime = "--:-- AM/PM";

    $scope.submitRequest = function(){
      requestService.submitRequest($scope.request);
      $scope.request ={};
      $state.go('myriderequests');
    };

    $scope.fromLocationChanged =function(val) {
      console.dir(val);
    };

    $scope.toLocationChanged =function(val) {
      console.dir(val);
    };


    function startTimePickerCallback(val) {
      if (typeof (val) === 'undefined') {
        console.log('Time not selected');
      } else {
        var selectedTime = new Date(val * 1000);
        $scope.startTimePicker.inputEpochTime = val;
        $scope.startTime = $scope.formatEpochTime(val);
        $scope.request.timeStart = $scope.formatEpochTime(val);
      }
    }

    function endTimePickerCallback(val) {
      if (typeof (val) === 'undefined') {
        console.log('Time not selected');
      } else {
        var selectedTime = new Date(val * 1000);
        $scope.endTimePicker.inputEpochTime = val;
        $scope.endTime = $scope.formatEpochTime(val);
        $scope.request.timeEnd = $scope.formatEpochTime(val);
      }
    }

    $scope.formatEpochTime = function(epoch) {
      var objDate = new Date(epoch * 1000);
      var meridian = (objDate.getUTCHours() >= 12) ? "PM" : "AM";
      var hours = (objDate.getUTCHours() > 12) ? ((objDate.getUTCHours() - 12)) : (objDate.getUTCHours());
      var minutes = (objDate.getUTCMinutes());

        minutes = (minutes < 10) ? ("0" + minutes) : (minutes);

        if (hours === 0 && meridian === "AM") {
          hours = 12;
        }
      return(hours + ':' + minutes + meridian);
    }

    $scope.startTimePicker = {
      inputEpochTime: ((new Date()).getHours() * 60 * 60),  //Optional
      step: 15,  //Optional
      format: 12,  //Optional
      titleLabel: '12-hour Format',  //Optional
      setLabel: 'Set',  //Optional
      closeLabel: 'Close',  //Optional
      setButtonType: 'button-positive',  //Optional
      closeButtonType: 'button-stable',  //Optional
      callback: function (val) {    //Mandatory
        startTimePickerCallback(val);
      }
    };

    $scope.endTimePicker = {
      inputEpochTime: ((new Date()).getHours() * 60 * 60),  //Optional
      step: 15,  //Optional
      format: 12,  //Optional
      titleLabel: '12-hour Format',  //Optional
      setLabel: 'Set',  //Optional
      closeLabel: 'Close',  //Optional
      setButtonType: 'button-positive',  //Optional
      closeButtonType: 'button-stable',  //Optional
      callback: function (val) {    //Mandatory
        endTimePickerCallback(val);
      }
    };

  }
})();

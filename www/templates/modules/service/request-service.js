(function() {
  angular.module('karmacar.serviceModule')
    .factory('requestService', ['userService', requestService]);

  function requestService(userService) {

    var factory = {};
    factory.submitRequest = function(rideRequest){
      rideRequest.id = userService.scope.myRideRequests.length + 1;
      userService.scope.myRideRequests.push(rideRequest);

    };

    factory.submitOffer = function(offer){
      offer.id = userService.scope.myRideOffers.length + 1;
      offer.status = "Pending";
      userService.scope.myRideOffers.push(offer);
       console.dir(userService.scope.myRideOffers);
    };

    factory.acceptOffer = function(offer){
      userService.scope.myRideOffers.forEach(function(rideOffer) {
        if(rideOffer.id === offer.id) {
          offer.status = "Accepted";
        }
      });
    };

    factory.confirmOffer = function(offer){
      userService.scope.myRideOffers.forEach(function(rideOffer) {
        if(rideOffer.id === offer.id) {
          offer.status = "Confirmed";
        }
      });
    };

    factory.cancelOffer = function(offer) {
      var index = -1;
      userService.scope.myRideOffers.forEach(function(rideOffer) {
        if(rideOffer.id === offer.id) {
          index = userService.scope.myRideOffers.indexOf(offer);
        }
      });
      if(index > -1) {
        userService.scope.myRideOffers.splice(index, 1);
      }
    };

    return factory;

  }

})();

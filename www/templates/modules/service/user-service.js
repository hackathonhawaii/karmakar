(function() {
  angular.module('karmacar.serviceModule')
    .factory('userService', [ userService]);

  function userService() {

    var scope = {};
      scope.status ={};
    scope.profile={
      "name": "John Doe",
      "profilePic": "../../../img/adam.jpg",
      "carModel": "1999 Ford Ranger XLT",
      "interests": ["Sailing", "Coding"]
    };
    scope.myRideRequests =[{
      "timeStart": "8:00pm",
      "timeEnd": "10:00pm",
      "requestDate": "3/11/2016",
      "from": "1680 Kapiolani Blvd, Honolulu, HI 96814",
      "to": "1000 Kamehameha Hwy #227-228, Pearl City, HI 96782",
      "karma": "50",
      "id": "2",
      "profilePic": "../../../img/blankAvatar.jpg",
      "comments": "Pick me UP!",
      "offers" : [{
        "time": "8:30",
        "location": "1680 Kapiolani Blvd, Honolulu, HI 96814",
        "comment": "jk"
      }, {
        "time": "9:30",
        "location": "1680 Kapiolani Blvd, Honolulu, HI 96814",
        "comment": "im not jk"
      }]
    }, {
      "timeStart": "3:30pm",
      "timeEnd": "4:30pm",
      "requestDate": "3/11/2016",
      "from": "850 Ticonderoga St Ste 100 Pearl Harbor, HI 96860",
      "to": "940 Queen St Honolulu, HI 96814",
      "karma": "30",
      "id": "3",
      "profilePic": "../../../img/blankAvatar.jpg",
      "comments": "Pick me UP! please :("
    }];
    scope.myRideOffers =[{
      "comments": "I might be a little late.",
      "id": "9999",
      "pickUpTime": "2:30pm",
      "pickupLocation": "Kahala Mall",
      "status" : "Confirmed"},
      {
        "comments": "Please bring coffee.",
        "id": "2000",
        "pickUpTime": "7:45pm",
        "pickupLocation": "Ala Moana Mall",
        "status" : "Done"

    }];

    return {
      scope: scope
    };

  }

})();

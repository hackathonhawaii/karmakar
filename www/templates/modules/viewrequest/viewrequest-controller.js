(function() {
  angular.module('karmacar.viewrequest')
    .controller('viewrequest-controller', ['$scope', '$stateParams', '$http', '$ionicHistory', 'userService', viewRequestController]);

  function viewRequestController($scope, $stateParams, $http, $ionicHistory, userService) {
    $scope.request ={};
    userService.scope.status.showProfile = {};

    function initialize() {
      $http.get('../mockdata/requestlist.json').then(function(result) {
        $scope.requests = result.data.Requests;
      }).then(function() {
        $scope.requests.forEach(function(request) {
          if(request.id === $stateParams.id) {
            $scope.request = request;
            userService.scope.status.showProfile = request.requesteeProfile;
            userService.scope.status.userRequest = request;
          }
        });
      });
    }

    $scope.goBack = function() {
      $ionicHistory.goBack();
    };

    initialize();
  }
})();
